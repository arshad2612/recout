<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailreset extends Mailable
{
    use Queueable, SerializesModels;
    public $token;
    public $email;
    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $email, $code)
    {
        $this->token = $token;
        $this->email = $email;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('Email.passwordReset')->with([
            'token' => $this->token,
            'email' => $this->email,
            'code' => $this->code
        ]);
    }
}
