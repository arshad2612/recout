<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $table = 'payments';
    
    protected $fillable = ['id', 'user_id','reservation_id','firstname','lastname','email','phone','card_name','card_number','cvc','expiry'];
}
