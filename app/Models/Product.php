<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

   
    //protected $fillable = ['id', 'name'];

    public function category(){
        return $this->blongsTo(Category::class);
    } 

    public function price(){
        return $this->hasMany(Price::class);
    }

    
}
