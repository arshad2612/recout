<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;


class Store extends Model
{
    use HasFactory;

    protected $table = 'stores';

    protected $fillable = ['id', 'name','location','contact_number','latitude','longitude'];
    
    public function products(){
        return $this->hasMany(Product::class);
    }
    
    public function category(){
        return $this->hasMany(Category::class);
    } 

    

    
   
    
}
