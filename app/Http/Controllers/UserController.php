<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Twilio\Jwt\ClientToken;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;


class UserController extends Controller
{
    
    //login function
    public function authenticate(Request $request)
    {
       //$credentials = $request->only('email', 'password'); 
        //$credentials = Arr::add($credentials, 'isVerified', '1');

        $credentials = [
            'email'         => $request->email,
            'password'      => $request->password,
            'isVerified'    => '1',
            'utype'         => 'user',
            'status'        => '1'
        ];

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = auth()->user();
        return response()->json([
            'message' => 'User successfully Login',
            'token' => $token,
            'user' =>$user
        ], 200);

    }

    // signup function
    public function register(Request $request)
    {
            $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            ]);

        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
        
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);
        $token = JWTAuth::fromUser($user);

        return response()->json([
            'message' => 'User successfully Registerd',
            'token' => $token,
            'user' =>$user
        ], 201);
    }


    // send varification code function
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'id'      => 'required',
            'country' => 'required',
            'phone_number' => 'required',
            ]);


            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
              
            $userID = $request->get('id');
            $user= User::find($userID);
            if($user){
                $code = rand(1000, 9999); //generate random code
                $request['code'] = $code; //add code in $request body
                $user->country  = $request->country;
                $user->phone_number = $request->phone_number;
                $user->verification_code  = $code;
                $user->save();
                
                if($this->sendSms($request)){
                    return response()->json([
                        'message' => 'Varification Code Send successfully'
                    ], 200); 
                }   
            }else{
                return response()->json([
                    'message' => 'Invalid User'
                ], 400); 
            }    


    }

    //send varification code sms
    public function sendSms($request)
    {
        
        $accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        try
        {
            $client = new Client(['auth' => [$accountSid, $authToken]]);
            $result = $client->post('https://api.twilio.com/2010-04-01/Accounts/'.$accountSid.'/Messages.json', 
            ['form_params' => [
                'Body' => 'CODE: '. $request->code, //set message body
                'To' => $request->phone_number,
                'From' => '+16415521529'    //we get this number from twilio
            ]]);
            return $result;
        }
        catch (Exception $e)
        {
            echo "Error: " . $e->getMessage();
        }
    }

    // verified code
    public function verifyCode(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'code' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $code = $request->get('code');
        $id = $request->get('id');

        $varified = User::where([
            ['id','=', $id],
            ['verification_code','=', $code]
        ])->latest()->first();

        if($varified){
            $varified->isVerified   = true;
            $varified->status   = true;
            $varified->save();
            return response()->json([
                'message' => 'Successfully Varified'
            ], 200);
           
        }else{
            return response()->json([
                'message' => 'Invalid Code'
            ], 400);
        }
        
    }

    //get current user information
    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                        return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                        return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                        return response()->json(['token_absent'], $e->getStatusCode());
        }
            return response()->json([
                'message' => 'Current User Information',
                'user' =>$user
            ], 201);
                
    }

    // update user profile
    public function update (Request $request, $id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'gender' => 'required',
            'email' => 'required|string|email|max:255',
            'location' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
 
        
        $url = url('/')."/storage/app/public/userImages/";
        $upload = '/public/userImages/';

        $data  = $request->file('photo'); 
        $extension = $request->photo->extension();
        $imageName1 = time().'.'.$extension ;
        $imageName = $url.$imageName1;
        $path = $request->photo->storeAs($upload, $imageName1);
        
        $user = User::find($id);
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->email  = $request->email;
        $user->location  = $request->location;
        $user->photo  = $imageName;
        $user->save();

        return response()->json([
            'message' => 'User profile successfully updated!'
        ], 200);   
    }


    //update user password
    public function update_password(Request $request,$id){
        
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed'
            
            ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user=User::find($id);
        
        $user->password=Hash::make($request->password);  
        $user->save();
        return response()->json([
            'message' => 'User Password Updated!'
        ], 200);   

    }

}
