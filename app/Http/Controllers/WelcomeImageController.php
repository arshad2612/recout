<?php

namespace App\Http\Controllers;
use App\Models\WelcomeImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WelcomeImageController extends Controller
{
    public function index(){
        
        $data = WelcomeImage::all(); 
        return response()->json([
            'message' => 'Get All Welcome Images',
            'videos' => $data
        ], 200);
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'image' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        //echo $ipAddress = $request->ip();
        //exit();

         $url = url('/')."/storage/app/public/welcomeImages/";
         $upload = '/public/welcomeImages/';
         $image      = $request->file('image');
         $filename   = time()."-".$image->getClientOriginalName();
         
         $imageName = $url.$filename;
         $path = $request->image->storeAs($upload, $filename);

        WelcomeImage::create([
            'title' => $request->title,
            'content' => $request->content,
            'image' => $imageName
        ]);

        return response()->json([
            'message' => 'Add Welcome Image Successfully Created!'
        ], 201);

    }
}
