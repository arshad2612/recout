<?php

namespace App\Http\Controllers;
use App\Models\Store;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;


class StoreController extends Controller
{
    // fetch all stores with products
    public function index(){
        
       $data = Store::where('status','accept')->with('products','products.price')->get();
       
        return response()->json([
            'message' => 'Get All Stores',
            'Stores' => $data
        ], 200);
        
    }

    // get store products
    public function storeProduct($id){
        
        $store= Store::where(['id' => $id, 'status' => 'accept'])->with('category','category.products','category.products.price')->get();
        //$store= Store::with("category")->->get();
        //$category = Category::with("products")->get();    
        return response()->json([
            'message' => 'Selected Store Products',
            'store' => $store,
            //'category' => $category,
         ], 201);

    }


    // get specific product detail
    public function productDetail($id){
        $product = Product::find($id);

        return response()->json([
            'message' => 'Show Product Detail',
            'Product Detail' => $product
         ], 201);



    }
}
