<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Reservation;
use App\Models\Payment;

class ReservationController extends Controller
{
    // show  current user reservations
    public function index($id){

        $data = Reservation::where('user_id', $id)->get();
        
        return response()->json([
            'message' => 'fetch user Reservation',
            'Reservation' => $data
        ], 200);
        
    }

    // create reservation
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'product_id' => 'required',
            'store_id' => 'required',
            'destination' => 'required',
            'checkin' => 'required|date',
            'checkout' => 'required|date|after:checkin',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $reservations = Reservation::create([
            'user_id' => $request->get('user_id'),
            'product_id' => $request->get('product_id'),
            'store_id' => $request->get('store_id'),
            'destination' => $request->get('destination'),
            'checkin' => $request->get('checkin'),
            'checkout' => $request->get('checkout')
        ]);
        

        return response()->json([
            'message' => 'Reservation successfully Create',
            'Reservation' =>$reservations
        ], 201);


    }

    // create payment function
    public function payment(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'reservation_id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|string|email|max:255|confirmed',
            'phone' => 'required',
            'card_name' => 'required',
            'card_number' => 'required',
            'cvc' => 'required',
            'expiry' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $payments = Payment::create([
            'user_id' => $request->get('user_id'),
            'reservation_id' => $request->get('reservation_id'),
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'card_name' => $request->get('card_name'),
            'card_number' => $request->get('card_number'),
            'cvc' => $request->get('cvc'),
            'expiry' => $request->get('expiry')
        ]);
        

        return response()->json([
            'message' => 'Payment successfully Create',
            'Reservation' =>$payments
        ], 201);


    }



}
