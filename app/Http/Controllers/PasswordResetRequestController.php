<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Mail\SendMailreset;
use Illuminate\Support\Facades\Mail;


class PasswordResetRequestController extends Controller
{
    public function sendPasswordResetEmail(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        if($this->validEmail($request->email)) {
            // If email exists
            $user = $this->validEmail($request->email);

            if($user){
                $code = rand(1000, 9999); //generate random code
                $request['code'] = $code; //add code in $request body
                $request['phone_number'] = $user->phone_number;
                
                $this->sendMail($request->email,$code);

                //$token = $this->generateToken($request->email, $code);
               
                return response()->json([
                        'message' => 'Varification Code Send successfully',
                        'user' =>$user
                ], Response::HTTP_OK); 
            }
          } else {
              //If email does not exist
              return response()->json([
                  'message' => 'Email does not exist.'
              ], Response::HTTP_NOT_FOUND);
          }
    }

    public function validEmail($email) {
        return User::where('email', $email)->first();
    }

    public function sendMail($email,$code){
        $token = $this->generateToken($email, $code);
        Mail::to($email)->send(new SendMailreset($token, $email, $code));
    }

    public function generateToken($email,$code){
        $isOtherToken = DB::table('password_resets')->where('email', $email)->first();
  
        if($isOtherToken) {
          return $isOtherToken->token;
        }
  
        $token = Str::random(80);;
        $this->storeToken($token, $email,$code);
        return $token;
      }
  
      public function storeToken($token, $email,$code){
          DB::table('password_resets')->insert([
              'email' => $email,
              'token' => $token,
              'verification_code' => $code,
              'created_at' => Carbon::now()            
          ]);
      }

    public function verifyCode(Request $request){

        $validator = Validator::make($request->all(), [
            'code' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $code = $request->get('code');

        $varified = DB::table('password_resets')->where('verification_code', $code)->latest()->first();
      
        if($varified){
            return response()->json([
                'message' => 'Successfully Varified',
                'user' => $varified 
            ], 200);
           
        }else{
            return response()->json([
                'message' => 'Invalid Verification Code'
            ], 400);
        }  
    }

    public function passwordResetProcess(Request $request){

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        return $this->updatePasswordRow($request)->count() > 0 ? $this->resetPassword($request) : $this->tokenNotFoundError();
    } 

    // Verify if token is valid
    private function updatePasswordRow($request){
        return DB::table('password_resets')->where('email',$request->email);
    }

    // Reset password
    private function resetPassword($request) {
        // find email
        $userData = User::whereEmail($request->email)->first();
        // update password
        $userData->update([
          'password'=>bcrypt($request->password)
        ]);
        // remove verification data from db
        $this->updatePasswordRow($request)->delete();

        // reset password response
         return response()->json([
           'data'=>'Password has been updated.'
         ],Response::HTTP_CREATED);
    }  
    
    // Token not found response  
    private function tokenNotFoundError() {
        return response()->json([
          'error' => 'Either your email or Verification Code is wrong.'
        ],Response::HTTP_UNPROCESSABLE_ENTITY);
    }




    









}
