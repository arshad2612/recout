@component('mail::message')
# {{ config('app.name') }}

Reset or change your password.

@component('mail::button', ['url' => ''])
<h1 style="color:white;">{{ $code }}</h1>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
