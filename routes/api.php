<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WelcomeImageController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\PasswordResetRequestController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



//**** without required token routes *****// 

//welcome image
Route::get('welcome', [WelcomeImageController::class, 'index']); 
Route::post('welcome',[WelcomeImageController::class, 'store']);

//signup 
Route::post('register', [UserController::class, 'register']); // create user Account
Route::post('sendcode', [UserController::class, 'store']);    // send varification sms code 
Route::post('verifycode', [UserController::class, 'verifyCode']);    // varified code
Route::post('login', [UserController::class, 'authenticate']); // login

//******forgot password******// 
Route::post('passwordReset', [PasswordResetRequestController::class, 'sendPasswordResetEmail']); // Send OTP
Route::post('passwprdverifycode', [PasswordResetRequestController::class, 'verifyCode']); // OTP verify
Route::post('updatePassword', [PasswordResetRequestController::class, 'passwordResetProcess']); // Update Password



Route::group(['middleware' => ['jwt.verify']], function() {
    
    //get single user information
    Route::get('user', [UserController::class, 'getAuthenticatedUser']); // get login user information
    Route::post('profile/{id}', [UserController::class, 'update']);      // update user profile

    //Store Api
    Route::get('store', [StoreController::class, 'index']); // show all stores
    Route::post('store', [StoreController::class, 'store']);  // create new store
    Route::get('store/{id}', [StoreController::class, 'storeProduct']); // show store produts
    Route::get('product/{id}', [StoreController::class, 'productDetail']); // show product detail
    Route::post('password/{id}', [UserController::class, 'update_password']);      // update password
    
    //Reservation APi
    Route::get('reservation/{id}', [ReservationController::class, 'index']); // show all stores
    Route::post('reservation', [ReservationController::class, 'store']);  // create new Reservation
    Route::post('payment', [ReservationController::class, 'payment']);  // create payment
    
});


